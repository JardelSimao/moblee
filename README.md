**LandingPage para teste prático - mobLee**

Adicionado cores chamativas e atraentes para facilitar na formação de novos leads.
O usuário pode escolher qual cor deseja através de um menu de configuração que fica no canto direito da tela.
Adicionei um novo formulário de contato, caso o usuário queira enviar uma mensagem.
A página é inteiramente responsiva.
Validação dos formulários (o envio não foi configurado, mas o ajax necessário esta previamente feito).
Fontes atraentes.
Efeito Paralax nas imagens de fundo, melhorando a visualização e tornando a experiência mais rica.

Enjoy.