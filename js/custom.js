jQuery(function () {
	"use strict";

	jQuery(document).ready(function(){

		$('.head-area').parallax("50%", 0.1);
		$('.call-to-action').parallax("50%", 0.1);

		
		jQuery(window).on('scroll', function() {
			parallax();
		});
		
		jQuery('a.learn-more').on('click', function() { // code
			jQuery('html, body').animate({ scrollTop:$('#service').offset().top - 50 }, 1000, // animation Speed
			function() {
				parallax();
			});
			return false;
		});

		function parallax() {
			 jQuery(window).scrollTop();
		}
		
	});
	
}());