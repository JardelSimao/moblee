$(document).ready(function() {

	 $('#contactform').validate({
	 	
		rules: {
	    	firstname: { required: true },
	    	secondname: { required: true },
	    	company: { required: true },
	    	email: { required: true, email: true }
		},
   			messages: {
     		firstname: "Por favor digite seu nome",
     		secondname: "Por favor digite seu sobrenome",
     		company: "Por favor digite o nome da empresa",
     		email: {
       			required: "Por favor digite seu email",
       			email: "O email precisa estar no formato: name@domain.com"
     		}
     	}

	});

	$('#contactform2').validate({
	 	
		rules: {
	    	firstname: { required: true },
	    	secondname: { required: true },
	    	message: { required: true },
	    	email: { required: true, email: true }
		},
   			messages: {
     		firstname: "Por favor digite seu nome",
     		secondname: "Por favor digite seu sobrenome",
     		message: "Por favor escreva a mensagem",
     		email: {
       			required: "Por favor digite seu email",
       			email: "O email precisa estar no formato: name@domain.com"
     		}
     	}

	});

	// $('#contactform').ajaxForm({
	// 	target: '#message',
	// 	url: 'lib/Simple-Form/mail.php',
	// 	success: successMessage,
	// 	clearForm: true,
	// 	resetForm: true
	// });
});

// function successMessage() {
// 	$('#message').fadeIn(500).delay(5000).fadeOut(500);
// }
